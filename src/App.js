import logo from "./logo.svg";
import "./App.css";
import NavBar from "./components/NavBar";
import { DndProvider } from "react-dnd";
import { HTML5Backend } from "react-dnd-html5-backend";
import { TouchBackend } from "react-dnd-touch-backend";
const options = {
  scrollAngleRanges: [
    { start: 30, end: 150 },
    { start: 210, end: 330 },
  ],
};

function App() {
  // if ("ontouchstart" in document.documentElement) {
  //   alert("handy");
  // } else {
  //   alert("desk");
  // }

  return (
    <div className="App">
      <DndProvider backend={HTML5Backend}>
        <NavBar />
      </DndProvider>
    </div>
  );
}

export default App;
