import React from "react";
const Modal = (props) => {
  const divStyle = {
    display: props.displayModal ? "block" : "none",
  };

  const { deleteItem, card } = props;

  function closeModal(e) {
    e.stopPropagation();
    props.closeModal();
  }
  return (
    <div className="modal" onClick={closeModal} style={divStyle}>
      {" "}
      <div className="modal-content" onClick={(e) => e.stopPropagation()}>
        {" "}
        <span className="close" onClick={closeModal}>
          &times;
        </span>{" "}
        <br />
        <h1 style={{ textAlign: "center" }}>Are you sure?</h1>
        <div
          style={{
            display: "flex",
            justifyContent: "space-between",
            marginRight: "12vw",
            marginLeft: "12vw",
            marginTop: "3rem",
          }}
        >
          <button
            className="btn-blue"
            onClick={(e) => {
              deleteItem(card);
              closeModal(e);
            }}
          >
            Yes
          </button>
          <button
            className="btn-gray"
            onClick={(e) => {
              closeModal(e);
            }}
          >
            No{" "}
          </button>
        </div>
      </div>{" "}
    </div>
  );
};
export default Modal;
