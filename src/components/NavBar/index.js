import { useState, useCallback } from "react";
import { Card } from "./Card";
import update from "immutability-helper";
import { act } from "react-dom/test-utils";
import Modal from "../Modal";

const style = {
  width: 400,
};
const NavBar = () => {
  {
    const [modal, setModal] = useState(false);
    const [delCard, setDelCard] = useState({ undefined });
    const selectModal = (card) => {
      setModal(!modal);
      if (card) {
        setDelCard(card);
      } else {
        setDelCard(undefined);
      }
    };
    const [cards, setCards] = useState([
      {
        id: 1,
        text: `Tab${1}`,
      },
      {
        id: 2,
        text: `Tab${2}`,
      },
      {
        id: 3,
        text: `Tab${3}`,
      },
    ]);
    const [active, setActive] = useState({
      id: 1,
      text: `Tab${1}`,
    });
    const addItem = () => {
      // for (let i = 0; i < cards.length; i++) {
      // }

      if (cards.length >= 10) {
        return;
      }

      let lengthIndexFilled = cards.some(
        (cardL) => cardL.id === cards[cards.length - 1].id
      );
      if (lengthIndexFilled) {
        setCards([
          ...cards,
          {
            id: cards.length + 1,
            text: `Tab${cards.length + 1}`,
          },
        ]);
      } else {
        setCards([...cards, { id: cards.length, text: `Tab${cards.length}` }]);
      }
    };
    const deleteItem = (card) => {
      if (cards.length > 1) {
        if (active === card) {
          handlePrev();
        }
        let new_list = cards.filter((cardL) => cardL !== card);
        let setList = new_list.map((card, i) => ({
          id: i + 1,
          text: `Tab${i + 1}`,
        }));

        setCards(setList);
      }
    };

    const handlePrev = () => {
      for (let i = 0; i < cards.length; i++) {
        if (cards[i].id === active.id && i > 0) {
          let tempCards = [...cards];
          let previous = tempCards[i - 1];
          // current = array[i];
          // next = array[i + 1]
          setActive(previous);
        }
      }
    };

    const handleNext = () => {
      for (let i = 0; i < cards.length; i++) {
        if (cards[i].id === active.id && i < cards.length - 1) {
          let tempCards = [...cards];
          let next = tempCards[i + 1];
          // current = array[i];
          // next = array[i + 1]
          setActive(next);
        }
      }
    };

    const moveCard = useCallback(
      (dragIndex, hoverIndex) => {
        const dragCard = cards[dragIndex];
        setCards(
          update(cards, {
            $splice: [
              [dragIndex, 1],
              [hoverIndex, 0, dragCard],
            ],
          })
        );
      },
      [cards]
    );
    const renderCard = (card, index, setActive, active) => {
      return (
        // <div
        //   style={{ width: "6rem", display: "flex", flexDirection: "column" }}
        // >
        <Card
          key={card.id}
          index={index}
          id={card.id}
          card={card}
          text={card.text}
          moveCard={moveCard}
          setActive={setActive}
          deleteItem={deleteItem}
          active={active}
          selectModal={selectModal}
        />

        // {card.id === active.id && (
        //   <div
        //     style={{
        //       border: "1px",
        //       borderStyle: "solid",
        //       borderColor: "#6dd5f5",
        //     }}
        //   ></div>
        // )}
        // </div>
      );
    };
    return (
      <div style={{ marginLeft: "10px", marginRight: "10px" }}>
        <h2>Demo Container</h2>
        {/* <p onClick={selectModal}>Open Modal</p> */}
        <Modal
          deleteItem={deleteItem}
          card={delCard}
          displayModal={modal}
          closeModal={selectModal}
        />
        <div style={{ display: "flex", overflowX: "auto" }}>
          <div onClick={handlePrev}>
            <i
              style={{
                color: active.id === cards[0].id && "gray",
              }}
              className="fas fa-chevron-left fa-xs clickable"
            ></i>
          </div>
          {cards.map((card, i) => renderCard(card, i, setActive, active))}
          <div style={{ marginLeft: "10px" }} onClick={handleNext}>
            <i
              style={{
                color: active.id === cards[cards.length - 1].id && "gray",
                marginLeft: "10px",
              }}
              className="fas fa-chevron-right fa-xs clickable"
            ></i>
          </div>

          <div
            style={{ marginLeft: "10px", paddingRight: "10px" }}
            onClick={addItem}
          >
            <i
              style={{ color: "gray", marginLeft: "8px" }}
              className="fas fa-plus fa-xs clickable"
            ></i>
          </div>
        </div>

        <div>
          <h1>Selected {active.text}</h1>
        </div>
      </div>
    );
  }
};

export default NavBar;
